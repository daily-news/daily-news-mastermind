package com.dailynews.mastermind.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.dailynews.mastermind.common.ConfluentProperties;

import avro.com.dailynews.mastermind.Article;

@Service
public class ArticleSender {

	@Autowired
	private ConfluentProperties properties;

	@Autowired
	private KafkaTemplate<String, Article> kafkaTemplate;

	public void send(Article article) {
		kafkaTemplate.send(properties.getConfluent().getKafka().getTopic().getAvro().toString(), article);
	}
}