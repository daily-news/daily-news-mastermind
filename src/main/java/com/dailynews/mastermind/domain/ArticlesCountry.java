package com.dailynews.mastermind.domain;

import lombok.Data;

@Data
public class ArticlesCountry {

	private Articles articlesList;
	private String country;
}
