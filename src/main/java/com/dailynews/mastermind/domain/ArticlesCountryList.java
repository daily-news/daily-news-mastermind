package com.dailynews.mastermind.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class ArticlesCountryList {

	private List<ArticlesCountry> articlesCountryList = new ArrayList<ArticlesCountry>();
}
