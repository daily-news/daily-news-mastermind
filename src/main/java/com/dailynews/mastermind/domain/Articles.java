package com.dailynews.mastermind.domain;

import lombok.Data;

import java.util.List;

import avro.com.dailynews.mastermind.Article;


@Data
public class Articles {

	private List<Article> articles;
}
