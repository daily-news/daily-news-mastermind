package com.dailynews.mastermind;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.dailynews.mastermind.batch.jobs.JobsProperties;
import com.dailynews.mastermind.common.ConfluentProperties;

@SpringBootApplication
@EnableConfigurationProperties({ ConfluentProperties.class, JobsProperties.class })
public class MastermindApplication {

	public static void main(String[] args) {
		SpringApplication.run(MastermindApplication.class, args);
	}
}
