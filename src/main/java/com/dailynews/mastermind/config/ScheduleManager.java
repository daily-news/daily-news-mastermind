package com.dailynews.mastermind.config;

import java.util.Date;

import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.dailynews.mastermind.batch.jobs.newsapi.NewsApiJob;
import com.dailynews.mastermind.common.MastermindConsts;

@Configuration
@EnableScheduling
public class ScheduleManager {

	@Autowired
	JobLauncher jobLauncher;

	@Bean
	JobParametersBuilder jobParametersbuilder() {
		return new JobParametersBuilder();
	};

	@Autowired
	NewsApiJob newsApiJob;

	@Scheduled(cron = MastermindConsts.CRON_SECS_10)
	public void runNewsApiJob() throws Exception {

		// @formatter:off
		jobLauncher.run(
			newsApiJob.job(
				newsApiJob.step1(newsApiJob.reader(), 
				newsApiJob.processor(), 
				newsApiJob.writer())
			),
			this.jobParametersbuilder().addDate(MastermindConsts.EXECUTION_DATE, new Date()).toJobParameters()
		);
		// @formatter:on
	}

}
