package com.dailynews.mastermind.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.dailynews.mastermind.batch.jobs.JobsProperties;
import com.dailynews.mastermind.batch.jobs.newsapi.NewsApiConst;
import com.dailynews.mastermind.common.CountriesConsts;
import com.dailynews.mastermind.domain.Articles;
import com.dailynews.mastermind.domain.ArticlesCountry;
import com.dailynews.mastermind.domain.ArticlesCountryList;

@Service
public class NewsApiService {

	@Autowired
	JobsProperties properties;

	@Autowired
	private RestTemplate restTemplate;

	public ArticlesCountryList getTrendingArticlesByCountry() {

		ArticlesCountryList articlesCountryList = new ArticlesCountryList();
		articlesCountryList.getArticlesCountryList().add(getBr());
		articlesCountryList.getArticlesCountryList().add(getEs());

		/*
		 * TODO: add rest of functions. This example is splicing functions into
		 * countries, but it's just for make everything easier. It suposes to be
		 * specific functions that can't be generalized.
		 */

		return articlesCountryList;
	}

	public ArticlesCountry getBr() {

		ArticlesCountry articlesCountry = new ArticlesCountry();

		// @formatter:off
		ResponseEntity<Articles> response = restTemplate.getForEntity(
			NewsApiConst.NEWS_API_BASE_URL + NewsApiConst.URL_BRAZIL + "&apiKey=" + properties.getJobs().getNewsapi().getApiKey(), 
			Articles.class
		);
		// @formatter:on

		articlesCountry.setArticlesList(response.getBody());
		articlesCountry.setCountry(CountriesConsts.BRAZIL);
		return articlesCountry;
	}

	public ArticlesCountry getEs() {

		ArticlesCountry articlesCountry = new ArticlesCountry();

		// @formatter:off
		ResponseEntity<Articles> response = restTemplate.getForEntity(
			NewsApiConst.NEWS_API_BASE_URL + NewsApiConst.URL_CHINA + "&apiKey=" + properties.getJobs().getNewsapi().getApiKey(), 
			Articles.class
		);
		// @formatter:on

		articlesCountry.setArticlesList(response.getBody());
		articlesCountry.setCountry(CountriesConsts.CHINA);
		return articlesCountry;
	}
}
