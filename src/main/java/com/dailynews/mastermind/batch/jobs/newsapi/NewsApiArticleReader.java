package com.dailynews.mastermind.batch.jobs.newsapi;

import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import com.dailynews.mastermind.domain.ArticlesCountryList;
import com.dailynews.mastermind.services.NewsApiService;

public class NewsApiArticleReader implements ItemReader<ArticlesCountryList> {

	@Autowired
	private NewsApiService newsApiService;

	private boolean isReaded = false;

	/*
	 * Spring Batch: readers must return null for evade being processed again.
	 */
	@Override
	public ArticlesCountryList read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		ArticlesCountryList articlesCountryList = newsApiService.getTrendingArticlesByCountry();

		for (int i = 0; i < articlesCountryList.getArticlesCountryList().size(); i++) {
			if ((i >= articlesCountryList.getArticlesCountryList().size()) || this.isReaded) {
				this.isReaded = false;
				return null;
			}
		}

		this.isReaded = true;
		return articlesCountryList;
	}
}
