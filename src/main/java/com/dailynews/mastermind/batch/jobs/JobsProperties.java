package com.dailynews.mastermind.batch.jobs;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class JobsProperties {

	private Jobs jobs;

	public Jobs getJobs() {
		return jobs;
	}

	public void setJobs(Jobs jobs) {
		this.jobs = jobs;
	}

	public static class Jobs {

		private Newsapi newsapi;

		public Newsapi getNewsapi() {
			return newsapi;
		}

		public void setNewsapi(Newsapi newsapi) {
			this.newsapi = newsapi;
		}

		public static class Newsapi {

			private String apiKey;

			public String getApiKey() {
				return apiKey;
			}

			public void setApiKey(String apiKey) {
				this.apiKey = apiKey;
			}

		}
	}
}
