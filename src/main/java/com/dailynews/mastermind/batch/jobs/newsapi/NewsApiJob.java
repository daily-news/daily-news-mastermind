package com.dailynews.mastermind.batch.jobs.newsapi;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.dailynews.mastermind.common.MastermindConsts;
import com.dailynews.mastermind.domain.ArticlesCountryList;

@Configuration
@EnableBatchProcessing
public class NewsApiJob implements JobExecutionListener {

	private static final Logger log = LoggerFactory.getLogger(NewsApiJob.class);

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Bean
	public ItemReader<ArticlesCountryList> reader() {
		return new NewsApiArticleReader();
	}

	@Bean
	public ItemWriter<ArticlesCountryList> writer() {
		return new NewsApiArticleWriter();
	}

	@Bean
	public ItemProcessor<ArticlesCountryList, ArticlesCountryList> processor() {
		return new NewsApiArticleProcessor();
	}

	@Bean
	public Job job(@Qualifier(MastermindConsts.STEP_1) Step step1) {

		return jobBuilderFactory.get(NewsApiConst.JOB_API_NEWS).start(step1).build();
	}

	@Bean
	@Qualifier(MastermindConsts.STEP_1)
	public Step step1(ItemReader<ArticlesCountryList> reader,
			ItemProcessor<ArticlesCountryList, ArticlesCountryList> processor, ItemWriter<ArticlesCountryList> writer) {

		// @formatter:off
		return stepBuilderFactory
					.get(MastermindConsts.STEP_1)
					.<ArticlesCountryList, ArticlesCountryList>chunk(100)
					.reader(reader)
					.processor(processor)
					.writer(writer)
					.build();
		// @formatter:on
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {

		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("SUCCESS");
		} else if (jobExecution.getStatus() == BatchStatus.FAILED) {
			log.error("ERROR");
		}

	}

	@Override
	public void afterJob(JobExecution jobExecution) {

		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			jobExecution.setEndTime(new Date());
			log.info("SUCCESS");
		} else if (jobExecution.getStatus() == BatchStatus.FAILED) {
			log.error("ERROR");
		}
	}

}
