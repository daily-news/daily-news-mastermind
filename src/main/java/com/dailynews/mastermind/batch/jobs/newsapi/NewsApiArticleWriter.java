package com.dailynews.mastermind.batch.jobs.newsapi;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.dailynews.mastermind.domain.ArticlesCountry;
import com.dailynews.mastermind.domain.ArticlesCountryList;
import com.dailynews.mastermind.kafka.ArticleSender;

import avro.com.dailynews.mastermind.Article;

public class NewsApiArticleWriter implements ItemWriter<ArticlesCountryList> {

	@Autowired
	private ArticleSender articleSender;

	@Override
	public void write(List<? extends ArticlesCountryList> articles) throws Exception {

		for (ArticlesCountryList articlesCountryList : articles) {
			for (ArticlesCountry articlesCountry : articlesCountryList.getArticlesCountryList()) {
				for (Article article : articlesCountry.getArticlesList().getArticles()) {
					articleSender.send(article);
				}
			}
		}
	}
}
