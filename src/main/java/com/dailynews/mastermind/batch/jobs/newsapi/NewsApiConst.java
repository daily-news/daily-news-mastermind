package com.dailynews.mastermind.batch.jobs.newsapi;

public class NewsApiConst {

	public static final String JOB_API_NEWS = "JOB_API_NEWS";

	public static final String NEWS_API_BASE_URL = "https://newsapi.org/v2/top-headlines";
	public static final String URL_BRAZIL = "?country=br";
	public static final String URL_CHINA = "?country=cn";

}
