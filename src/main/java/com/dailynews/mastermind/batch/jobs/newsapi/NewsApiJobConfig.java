package com.dailynews.mastermind.batch.jobs.newsapi;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.JobParametersValidator;
import org.springframework.batch.core.launch.support.RunIdIncrementer;

public class NewsApiJobConfig extends RunIdIncrementer implements Job {

	@Override
	public String getName() {
		return NewsApiConst.JOB_API_NEWS;
	}

	@Override
	public boolean isRestartable() {
		return false;
	}

	@Override
	public void execute(JobExecution execution) {

	}

	@Override
	public JobParametersIncrementer getJobParametersIncrementer() {
		return new RunIdIncrementer();
	}

	@Override
	public JobParametersValidator getJobParametersValidator() {
		return null;
	}

}
