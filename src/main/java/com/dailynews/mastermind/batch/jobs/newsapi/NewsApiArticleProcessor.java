package com.dailynews.mastermind.batch.jobs.newsapi;

import org.springframework.batch.item.ItemProcessor;


import com.dailynews.mastermind.domain.ArticlesCountryList;

import avro.com.dailynews.mastermind.Article;

import com.dailynews.mastermind.domain.ArticlesCountry;
import com.dailynews.mastermind.domain.Articles;

public class NewsApiArticleProcessor implements ItemProcessor<ArticlesCountryList, ArticlesCountryList> {

	@Override
	public ArticlesCountryList process(ArticlesCountryList articlesCountryList) throws Exception {

		for (ArticlesCountry articlesCountry : articlesCountryList.getArticlesCountryList()) {

			Articles articles = articlesCountry.getArticlesList();
			for (Article article : articles.getArticles()) {
				article.setCountry(articlesCountry.getCountry());
			}

		}

		return articlesCountryList;
	}
}
