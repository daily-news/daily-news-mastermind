package com.dailynews.mastermind.common;

public class MastermindConsts {

	/*
	 * Steps
	 */
	public static final String STEP_1 = "STEP_1";

	/*
	 * Crons
	 */
	public static final String CRON_SECS_59 = "*/59 * * * * *";
	public static final String CRON_SECS_10 = "*/10 * * * * *";

	/*
	 * Others
	 */
	public static final String EXECUTION_DATE = "execution_date";
}
