package com.dailynews.mastermind.common;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class ConfluentProperties {

	private Confluent confluent;

	public Confluent getConfluent() {
		return confluent;
	}

	public void setConfluent(Confluent confluent) {
		this.confluent = confluent;
	}

	public static class Confluent {

		private Kafka kafka;

		public Kafka getKafka() {
			return kafka;
		}

		public void setKafka(Kafka kafka) {
			this.kafka = kafka;
		}

		public static class Kafka {

			private String bootstrapServers;
			private Topic topic;

			public String getBootstrapServers() {
				return bootstrapServers;
			}

			public void setBootstrapServers(String bootstrapServers) {
				this.bootstrapServers = bootstrapServers;
			}

			public Topic getTopic() {
				return topic;
			}

			public void setTopic(Topic topic) {
				this.topic = topic;
			}

			public static class Topic {

				private String avro;

				public String getAvro() {
					return avro;
				}

				public void setAvro(String avro) {
					this.avro = avro;
				}

			}
		}
	}

}
