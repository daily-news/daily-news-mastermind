# Daily News: Mastermind

> The project reads from an external API and insert into Kafka via Spring-batch jobs

# Generate Avro classes
> mvn generate-sources

> Right click over folder "target/generated" -> use as source folder

> Now take Article.java and replace the package if needed for compiling
